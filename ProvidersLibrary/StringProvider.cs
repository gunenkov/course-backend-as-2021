﻿using System.Threading.Tasks;
using ProvidersLibrary.Abstractions;

namespace ProvidersLibrary
{
    public class StringProvider : IDataProvider
    {
        Task<object> IDataProvider.ProvideNext()
        {
            return Task.FromResult("Good" as object);
        }
    }
}