﻿using System;

namespace ProvidersLibrary.Abstractions
{
    /// <summary>
    ///     Определяет объект, позволяющий создавать экземпляры провайдеров необходимого типа
    /// </summary>
    public interface IDataProviderFactory
    {
        /// <summary>
        ///     Метод создает объект провайдера необходимого типа. При реализации используется
        ///     рефлексия - название провайдера должно соответствовать шаблону {Type}Provider
        /// </summary>
        /// <param name="type">Тип провайдера</param>
        /// <returns>Объект провайдера необходимого типа</returns>
        IDataProvider Create(Type type);
    }
}