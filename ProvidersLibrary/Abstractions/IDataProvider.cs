﻿using System.Threading.Tasks;

namespace ProvidersLibrary.Abstractions
{
    /// <summary>
    ///     Определяет функциональность провайдера данных - объекта, получающего некоторые
    ///     входные данные и преобразовывающего их в определенные выходные данные
    /// </summary>
    public interface IDataProvider
    {
        /// <summary>
        ///     Метод получает следующий объект данных для отправки подписчикам
        /// </summary>
        /// <returns>Следующий обхект данных для отправки подписчикам</returns>
        Task<object> ProvideNext();
    }
}