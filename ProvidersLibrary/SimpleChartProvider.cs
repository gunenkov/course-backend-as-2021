﻿using System.Threading.Tasks;
using ProvidersLibrary.Abstractions;
using ProvidersLibrary.Models;

namespace ProvidersLibrary
{
    public class SimpleChartProvider : IDataProvider
    {
        public Task<object> ProvideNext()
        {
            // имитация преобразования входных данных

            // TODO требуемое преобразование входных данных в выходные
            return Task.FromResult(new SimpleChart() as object);
        }
    }
}