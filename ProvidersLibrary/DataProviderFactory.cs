﻿using System;
using System.Linq;
using System.Reflection;
using ProvidersLibrary.Abstractions;
using ProvidersLibrary.Exceptions;

namespace ProvidersLibrary
{
    public class DataProviderFactory : IDataProviderFactory
    {
        public IDataProvider Create(Type type)
        {
            var availableProviderTypes = Assembly.GetAssembly(GetType()).GetTypes()
                .Where(t => t.IsAssignableTo(typeof(IDataProvider)) && !t.IsAbstract).ToList();

            var providerType = availableProviderTypes.FirstOrDefault(t => t.Name == $"{type.Name}Provider");
            if (providerType == null)
                throw new ProviderNotFoundException("Провайдер данных указанного типа не найден в сборке");

            return Activator.CreateInstance(providerType) as IDataProvider;
        }
    }
}