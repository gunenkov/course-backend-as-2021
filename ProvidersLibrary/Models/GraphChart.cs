﻿using System;

namespace ProvidersLibrary.Models
{
    public class GraphChart
    {
        public Graphfact[] GraphFact { get; set; }
        public Graphplan[] GraphPlan { get; set; }

        public class Graphfact
        {
            public float Value { get; set; }
            public DateTime TimeStamp { get; set; }
        }

        public class Graphplan
        {
            public int Value { get; set; }
            public DateTime TimeStamp { get; set; }
        }
    }
}