﻿using System;

namespace ProvidersLibrary.Exceptions
{
    internal class ProviderNotFoundException : Exception
    {
        public ProviderNotFoundException(string message)
            : base(message)
        {
        }
    }
}