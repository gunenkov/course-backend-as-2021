﻿using System;
using System.Linq;

namespace CyclesTask
{
    public static class NotationHelper
    {
        /// <summary>
        ///     Метод принимает массив из 0 и 1, например {0, 1, 1, 0}, рассматривает полученную совокупность цифр
        ///     как число в двоичной системе счисления и возвращает десятичное представление числа
        /// </summary>
        /// <param name="binaryArray">Массив из 0 и 1, представляющий число в двоичной системе счисления</param>
        /// <returns>Десятичное представление числа</returns>
        public static int BinaryArrayToNumber(this int[] binaryArray)
        {
            return (int) binaryArray.Select((digit, i) => digit * Math.Pow(2, binaryArray.Length - 1 - i)).Sum();
        }
    }
}