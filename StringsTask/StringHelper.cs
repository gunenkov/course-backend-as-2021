﻿using System.Linq;
using System.Text.RegularExpressions;

namespace StringsTask
{
    public static class StringHelper
    {
        /// <summary>
        ///     Метод принимает строку с обратным порядком букв в словах, а возвращает строку CamelCase
        /// </summary>
        /// <param name="str">Исходная строка</param>
        /// <returns>Строка с правильным порядком букв в словах и примененным CamelCase</returns>
        public static string ReverseWordsAndCamelCase(string str)
        {
            // разворачиваем слова и склеиваем их в строку с пробелом в качестве разделителя
            var reversedByWordsString = string.Join(' ', str.Split()
                .Select(word => new string(word.Reverse().ToArray())));
            /* с помощью регулярного выражения ищем первые буквы слов и заменяем их на результат функции,
              которая каждому найденному совпадению ставит в соответствие результат метода ToUpper */
            return Regex.Replace(reversedByWordsString, "(^\\w)|(\\s\\w)", m => m.Value.ToUpper())
                .Replace(" ", string.Empty);
        }
    }
}