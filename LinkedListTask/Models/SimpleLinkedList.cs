﻿namespace LinkedListTask.Models
{
    public class SimpleLinkedList
    {
        public Node Head { get; set; }

        /// <summary>
        ///     Метод, реализующий алгоритм Флойда (черепахи и зайца) для ответа на вопрос о наличии цикла в связном списке
        /// </summary>
        /// <returns>Ответ на вопрос: есть ли в связном списке цикл</returns>
        public bool IsCycleExists()
        {
            var turtlePosition = Head;
            var harePosition = Head.Next;

            while (turtlePosition != null && harePosition != null)
            {
                if (turtlePosition.Equals(harePosition)) return true;

                if (turtlePosition.Next != null && harePosition.Next != null)
                {
                    // черепаха двигается на одну позицию вперед
                    turtlePosition = turtlePosition.Next;
                    // заяц двигается на две позиции вперед
                    harePosition = harePosition.Next.Next;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }
    }
}