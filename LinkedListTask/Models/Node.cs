﻿namespace LinkedListTask.Models
{
    public class Node
    {
        public Node Next { get; set; }
        public object Data { get; set; }
    }
}