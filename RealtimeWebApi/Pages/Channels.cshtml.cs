using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RealtimeWebApi.Channels.Abstractions;
using RealtimeWebApi.Services.Abstractions;

namespace RealtimeWebApi.Pages
{
    public class ChannelsModel : PageModel
    {
        private readonly IChannelsService channelsService;

        public ChannelsModel(IChannelsService channelsService)
        {
            this.channelsService = channelsService;
        }

        public string Title { get; set; } = "������ �������";
        public IList<Channel> Channels { get; set; } = new List<Channel>();

        public void OnGet()
        {
            Channels = channelsService.GetChannels();
        }
    }
}