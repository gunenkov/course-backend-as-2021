﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using ProvidersLibrary.Abstractions;
using RealtimeWebApi.Extensions.Utils;

namespace RealtimeWebApi.Channels.Abstractions
{
    /// <summary>
    ///     Определяет объект канала с неопределенным типом
    /// </summary>
    public abstract class Channel : Hub
    {
        private readonly IDataProvider dataProvider;

        protected Channel(IDataProviderFactory dataProviderFactory, Type type)
        {
            dataProvider = dataProviderFactory.Create(type);
            ChannelType = type;
        }

        public Guid ChannelId { get; } = Guid.NewGuid();
        public Type ChannelType { get; }
        public string ChannelTypeName => ChannelType.Name.ToChannelTypeName();

        /// <summary>
        ///     Метод выполняет отправку поступивших данных всем подписчикам.
        ///     Предназначен для отправки данных при непосредственном клиентском запросе
        /// </summary>
        /// <returns></returns>
        public async Task BroadcastOnRequest(object input)
        {
            var body = new {ChannelId, Type = ChannelTypeName, Data = input};

            await Clients.All.SendAsync(GetType().Name,
                JsonSerializer.Serialize(body,
                    new JsonSerializerOptions {PropertyNamingPolicy = JsonNamingPolicy.CamelCase}));
        }

        /// <summary>
        ///     Метод выполняет подготовку данных из провайдера для отправки подписчикам
        ///     в необходимом формате. Предназначен для работы с контекстом канала в фоновой службе
        /// </summary>
        /// <returns>Задача, результатом которой является JSON строка</returns>
        public async Task<string> CreateMessageFromProvider()
        {
            var body = new {ChannelId, Type = ChannelTypeName, Data = await dataProvider.ProvideNext()};
            return JsonSerializer.Serialize(body,
                new JsonSerializerOptions {PropertyNamingPolicy = JsonNamingPolicy.CamelCase});
        }
    }
}