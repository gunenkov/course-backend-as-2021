﻿using ProvidersLibrary.Abstractions;
using ProvidersLibrary.Models;
using RealtimeWebApi.Channels.Abstractions;

namespace RealtimeWebApi.Channels
{
    public class SimpleChartChannel : Channel
    {
        public SimpleChartChannel(IDataProviderFactory dataProviderFactory)
            : base(dataProviderFactory, typeof(SimpleChart))
        {
        }
    }
}