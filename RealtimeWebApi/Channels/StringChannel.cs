﻿using ProvidersLibrary.Abstractions;
using RealtimeWebApi.Channels.Abstractions;

namespace RealtimeWebApi.Channels
{
    public class StringChannel : Channel
    {
        public StringChannel(IDataProviderFactory dataProviderFactory)
            : base(dataProviderFactory, typeof(string))
        {
        }
    }
}