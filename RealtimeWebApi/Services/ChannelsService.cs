﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using RealtimeWebApi.Channels.Abstractions;
using RealtimeWebApi.Services.Abstractions;

namespace RealtimeWebApi.Services
{
    public class ChannelsService : IChannelsService
    {
        private readonly IServiceProvider serviceProvider;

        public ChannelsService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IList<Channel> GetChannels()
        {
            var channels = new List<Channel>();

            var channelTypes = Assembly.GetAssembly(GetType()).GetTypes()
                .Where(t => !t.IsAbstract && t.IsAssignableTo(typeof(Channel))).ToList();

            foreach (var channelType in channelTypes)
            {
                var channel = serviceProvider.GetService(channelType) as Channel;
                if (channel != null) channels.Add(channel);
            }

            return channels;
        }
    }
}