﻿using System.Collections.Generic;
using RealtimeWebApi.Channels.Abstractions;

namespace RealtimeWebApi.Services.Abstractions
{
    public interface IChannelsService
    {
        IList<Channel> GetChannels();
    }
}