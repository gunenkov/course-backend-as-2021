﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using ProvidersLibrary;
using ProvidersLibrary.Abstractions;
using RealtimeWebApi.Channels;
using RealtimeWebApi.HostedServices;

namespace RealtimeWebApi.Extensions.Services
{
    public static class ChannelExtensions
    {
        /// <summary>
        ///     Метод предназначен для добавления каналов и обслуживающих
        ///     сервисов в DI контейнер
        /// </summary>
        /// <param name="services"></param>
        public static void AddChannels(this IServiceCollection services)
        {
            services.AddSingleton<IDataProviderFactory, DataProviderFactory>();

            services.AddSingleton<StringChannel>();
            services.AddHostedService<BackgroundBroadcastService<StringChannel>>();

            services.AddSingleton<SimpleChartChannel>();
            services.AddHostedService<BackgroundBroadcastService<SimpleChartChannel>>();

            //TODO Можно добавить другие каналы с указанием типа
        }

        /// <summary>
        ///     Метод предназначен для соотнесения добавленных каналов с маршрутами
        /// </summary>
        /// <param name="endpoints"></param>
        public static void MapChannels(this IEndpointRouteBuilder endpoints)
        {
            endpoints.MapHub<StringChannel>("/string");
            endpoints.MapHub<SimpleChartChannel>("/chart");

            //TODO Можно маппить другие каналы с указанием типа
        }
    }
}