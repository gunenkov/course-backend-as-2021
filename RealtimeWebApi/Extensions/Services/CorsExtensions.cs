﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace RealtimeWebApi.Extensions.Services
{
    public static class CorsExtensions
    {
        public static void AddDefaultCorsPolicy(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors(options => options.AddDefaultPolicy(builder => builder
                .SetIsOriginAllowedToAllowWildcardSubdomains()
                .WithOrigins(configuration.GetSection("Cors:Origins").Get<string[]>())
                .AllowAnyMethod()
                .AllowCredentials()
                .AllowAnyHeader()
            ));
        }
    }
}