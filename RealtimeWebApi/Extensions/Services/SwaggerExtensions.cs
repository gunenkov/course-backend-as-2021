﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace RealtimeWebApi.Extensions.Services
{
    public static class SwaggerExtensions
    {
        public static void AddSwaggerDoc(this IServiceCollection services)
        {
            services.AddSwaggerGen(options => options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "RealtimeWebApi",
                Description = "Api developed during AS course 2021",
                Contact = new OpenApiContact
                {
                    Name = "Mikhail Gunenkov",
                    Url = new Uri("https://vk.com/m_gunenkov")
                }
            }));
        }

        public static void UseSwaggerDoc(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "RealtimeWebApi"));
        }
    }
}