﻿using System.Linq;

namespace RealtimeWebApi.Extensions.Utils
{
    public static class StringExtensions
    {
        public static string ToChannelTypeName(this string str)
        {
            return string.Concat(str.Select((x, i) => i > 0 && char.IsUpper(x) ? "-" + x : x.ToString())).ToLower();
        }
    }
}