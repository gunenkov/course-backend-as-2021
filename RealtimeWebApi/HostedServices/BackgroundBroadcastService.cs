﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using RealtimeWebApi.Channels.Abstractions;

namespace RealtimeWebApi.HostedServices
{
    /// <summary>
    ///     Фонова служба, осуществляющая рассылку сообщений клиентам по указанному каналу
    /// </summary>
    /// <typeparam name="T">Канал</typeparam>
    public class BackgroundBroadcastService<T> : BackgroundService where T : Channel
    {
        private readonly T channel;
        private readonly IHubContext<T> channelContext;
        private readonly IConfiguration configuration;

        public BackgroundBroadcastService(IConfiguration configuration, T channel, IHubContext<T> channelContext)
        {
            this.configuration = configuration;
            this.channel = channel;
            this.channelContext = channelContext;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await channelContext.Clients.All.SendAsync(typeof(T).Name, await channel.CreateMessageFromProvider(),
                    stoppingToken);
                await Task.Delay(Convert.ToInt32(configuration["Channels:ResendInterval"]), stoppingToken);
            }
        }
    }
}