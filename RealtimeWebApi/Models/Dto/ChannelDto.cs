﻿using System;

namespace RealtimeWebApi.Models.Dto
{
    public class ChannelDto
    {
        public Guid ChannelId { get; set; }
        public string ChannelTypeName { get; set; }
    }
}