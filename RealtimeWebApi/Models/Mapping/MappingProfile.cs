﻿using AutoMapper;
using RealtimeWebApi.Channels.Abstractions;
using RealtimeWebApi.Models.Dto;

namespace RealtimeWebApi.Models.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Channel, ChannelDto>();
        }
    }
}