using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RealtimeWebApi.Extensions.Services;
using RealtimeWebApi.Services;
using RealtimeWebApi.Services.Abstractions;

namespace RealtimeWebApi
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IHostEnvironment environment)
        {
            var configurationBuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", true, true);
            configuration = configurationBuilder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddRazorPages();
            services.AddSignalR();
            services.AddAutoMapper(GetType().Assembly);
            services.AddChannels();
            services.AddSwaggerDoc();
            services.AddSingleton<IChannelsService, ChannelsService>();
            services.AddDefaultCorsPolicy(configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseSwaggerDoc();
            app.UseStaticFiles();
            app.UseRouting();

            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapChannels();
            });
        }
    }
}