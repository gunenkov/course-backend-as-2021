﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RealtimeWebApi.Models.Dto;
using RealtimeWebApi.Services.Abstractions;

namespace RealtimeWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChannelsController : ControllerBase
    {
        private readonly IChannelsService channelsService;
        private readonly IMapper mapper;

        public ChannelsController(IChannelsService channelsService, IMapper mapper)
        {
            this.channelsService = channelsService;
            this.mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult> InvokeChannel(Guid channelGuid, object input)
        {
            var channel = channelsService.GetChannels().FirstOrDefault(c => c.ChannelId == channelGuid);

            if (channel == null) return NotFound();

            await channel.BroadcastOnRequest(input);
            return Ok();
        }

        [HttpGet]
        public ActionResult<IList<ChannelDto>> GetChannels()
        {
            return Ok(mapper.Map<IList<ChannelDto>>(channelsService.GetChannels()));
        }
    }
}