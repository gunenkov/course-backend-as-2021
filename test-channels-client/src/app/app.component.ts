import {Component} from '@angular/core';
import * as signalR from '@aspnet/signalr';
import {HubConnection} from '@aspnet/signalr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public url: string = "";
  public methodName: string = "";
  public output: string = "";
  public data: string = "";

  public activeConnection: HubConnection;

  title = 'Тестирование каналов';

  connectToChannel() {
    if (this.activeConnection) return;
    if (this.url.length == 0 || this.methodName.length == 0) return;
    const connection = new signalR.HubConnectionBuilder()
      .withUrl(this.url)
      .build();

    connection.start()
      .then(() => {
        this.output = this.output.concat("Подключено!\n");
        this.activeConnection = connection;
      })
      .catch(err => this.output = this.output.concat("Ошибка при подключении: ").concat(err).concat("\n"));

    console.log();

    connection.on(this.methodName,
      data => {
        this.output = this.output.concat(data + "\n");
      });
  }

  disconnectFromChannel() {
    if (this.activeConnection) {
      this.activeConnection.stop().then(() => {
          this.output = this.output.concat("Отключено!\n");
          this.activeConnection = null;
        }
      )
    }
  }

  clearOutput() {
    this.output = "";
  }

  sendRequest() {
    if (this.activeConnection) {
      this.activeConnection.invoke("BroadcastOnRequest", JSON.parse(this.data)).catch(err => {
        this.output = this.output.concat("Произошла ошибка при отправке данных: ".concat(err))
      });
    }
  }
}
