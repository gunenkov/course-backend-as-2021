﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using JsonLinqTask.Models;

namespace JsonLinqTask.Parsers
{
    public static class DealJsonParser
    {
        /// <summary>
        ///     Метод получает коллекцию объектов типа Deal из файла JSON
        /// </summary>
        /// <param name="filename">Имя файла JSON, из которого необходимо получить объекты</param>
        /// <returns>Абстрактная коллекция объектов</returns>
        public static async Task<IList<Deal>> ParseDealsAsync(string filename)
        {
            var deals = new List<Deal>();
            await using (var stream = new FileStream(filename, FileMode.Open))
            {
                deals = await JsonSerializer.DeserializeAsync<List<Deal>>(stream);
            }

            return deals;
        }

        /// <summary>
        ///     Метод фильтрует сделки по сумме (не меньше 100 рублей), берет три сделки с самой ранней датой,
        ///     возвращает номера Id сделок
        /// </summary>
        /// <param name="deals">Коллекция объектов типа Deal, из которой осуществляется выборка</param>
        /// <returns>Список номеров сделок</returns>
        public static IList<string> GetNumbersOfDeals(IList<Deal> deals)
        {
            return deals.Where(deal => deal.Sum >= 100).OrderBy(deal => deal.Date).Take(3).Select(deal => deal.Id)
                .ToList();
        }
    }
}