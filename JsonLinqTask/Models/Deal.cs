﻿using System;

namespace JsonLinqTask.Models
{
    public class Deal
    {
        public int Sum { get; set; }
        public string Id { get; set; }
        public DateTime Date { get; set; }
    }
}