using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DockerWebApi2
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IHostEnvironment environment)
        {
            var configurationBuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", true, true);
            configuration = configurationBuilder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<HttpClient>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, HttpClient client,
            IConfiguration configuration)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseEndpoints(endpoints => endpoints.MapGet("/", async context =>
            {
                var response = await client.GetAsync(configuration["DockerWebApi1Url"]);
                await context.Response.WriteAsync($"Hello, {await response.Content.ReadAsStringAsync()}!");
            }));
        }
    }
}