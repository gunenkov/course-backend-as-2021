﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JsonLinqTask.Models;
using JsonLinqTask.Parsers;
using Newtonsoft.Json;
using Xunit;

namespace UnitTests.AditionalTasks2
{
    public class TestDealJsonParser
    {
        public TestDealJsonParser()
        {
            if (!File.Exists("JSON_sample_1.json"))
                File.WriteAllText("JSON_sample_1.json", JsonConvert.SerializeObject(new List<Deal>
                {
                    new Deal
                    {
                        Date = DateTime.Parse("2019-02-27T07:52:54.168Z"),
                        Id = "ABC12123",
                        Sum = 12
                    },
                    new Deal
                    {
                        Date = DateTime.Parse("2019-02-25T07:53:41.132Z"),
                        Id = "ABC12343",
                        Sum = 34
                    },
                    new Deal
                    {
                        Date = DateTime.Parse("2019-01-25T08:32:01.813Z"),
                        Id = "ABC1212343",
                        Sum = 1234
                    },
                    new Deal
                    {
                        Date = DateTime.Parse("2019-01-12T07:17:24.167Z"),
                        Id = "ABC1234213",
                        Sum = 3412
                    },
                    new Deal
                    {
                        Date = DateTime.Parse("2019-01-22T17:01:52.421+03:00"),
                        Id = "ABC1243213",
                        Sum = 4321
                    },
                    new Deal
                    {
                        Date = DateTime.Parse("2019-01-22T17:01:52.421Z"),
                        Id = "ABC1243213",
                        Sum = 4322
                    },
                    new Deal
                    {
                        Date = DateTime.Parse("2019-01-22T17:01:52.421-03:00"),
                        Id = "ABC1243213",
                        Sum = 4323
                    }
                }));
        }

        [Fact]
        public async Task FirstTest()
        {
            var deals = await DealJsonParser.ParseDealsAsync("JSON_sample_1.json");
            Assert.Equal(7, deals.Count);
        }

        [Fact]
        public async Task SecondTest()
        {
            var deals = await DealJsonParser.ParseDealsAsync("JSON_sample_1.json");
            var deal = deals.First(d => d.Sum == 4321);
            Assert.Equal("ABC1243213", deal.Id);
        }

        [Fact]
        public async Task ThirdTest()
        {
            var deals = await DealJsonParser.ParseDealsAsync("JSON_sample_1.json");
            var filteredDeals = DealJsonParser.GetNumbersOfDeals(deals);
            Assert.Equal(3, filteredDeals.Count);
            Assert.Equal("ABC1234213", filteredDeals[0]);
        }
    }
}