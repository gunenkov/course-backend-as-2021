﻿using System.Linq;
using System.Threading.Tasks;
using PatternsTask.Builder;
using Xunit;

namespace UnitTests.BuilderPattern
{
    public class TestUserHttpService
    {
        [Fact]
        public async Task TestBuilder()
        {
            var brigades = Enumerable.Range(1, 10).Select(id => new BrigadeDto {Id = id}).ToArray();
            var users = Enumerable.Range(1, 20).Select(id => new User {Id = id}).ToArray();


            var userHttpService = new UserHttpServiceBuilder().WithGetAllBrigadesAsync(brigades)
                .WithGetAllUsersAsync(users).WithGetBrigadeById(brigades[8])
                .WithGetUserById(users[14]).Build();

            Assert.Equal(await userHttpService.GetAllBrigades(), brigades);
            Assert.Equal(await userHttpService.GetAllUsers(), users);
            Assert.Equal(await userHttpService.GetBrigadeById(9), brigades[8]);
            Assert.Equal(await userHttpService.GetUserById(15), users[14]);
        }
    }
}