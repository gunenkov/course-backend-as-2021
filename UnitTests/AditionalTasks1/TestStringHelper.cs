﻿using StringsTask;
using Xunit;

namespace UnitTests.AditionalTasks1
{
    public class TestStringHelper
    {
        [Fact]
        public void StringTest()
        {
            Assert.Equal("ThisIsAnExample", StringHelper.ReverseWordsAndCamelCase("siht si na elpmaxe"));
        }
    }
}