﻿using CyclesTask;
using Xunit;

namespace UnitTests.AditionalTasks1
{
    public class TestNotationHelper
    {
        [Fact]
        public void FirstTest()
        {
            var test1 = new[] {0, 0, 0, 0};
            var test2 = new[] {1, 1, 1, 1};
            var test3 = new[] {0, 1, 1, 0};
            var test4 = new[] {0, 1, 0, 1};

            Assert.Equal(0, test1.BinaryArrayToNumber());
            Assert.Equal(15, test2.BinaryArrayToNumber());
            Assert.Equal(6, test3.BinaryArrayToNumber());
            Assert.Equal(5, test4.BinaryArrayToNumber());
        }
    }
}