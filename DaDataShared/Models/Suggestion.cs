﻿using System.Text.Json.Serialization;

namespace DaDataShared.Models
{
    public class Suggestion
    {
        [JsonPropertyName("value")] public string Value { get; set; }
    }
}