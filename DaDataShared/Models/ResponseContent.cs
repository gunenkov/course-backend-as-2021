﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DaDataShared.Models
{
    public class ResponseContent
    {
        [JsonPropertyName("suggestions")] public IEnumerable<Suggestion> Suggestions { get; set; }
    }
}