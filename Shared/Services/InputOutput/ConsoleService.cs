﻿using System;
using Shared.Services.InputOutput.Abstractions;

namespace Shared.Services.InputOutput
{
    public class ConsoleService : InputOutputService
    {
        public override event LogMessage ClientInput;

        public override void WriteLine(string str)
        {
            Console.WriteLine(str);
        }

        public override string ReadLine()
        {
            var clientInput = Console.ReadLine();
            ClientInput?.Invoke($"Пользователь ввел строку \"{clientInput}\". Отметка времени: {DateTime.Now}");
            return clientInput;
        }
    }
}