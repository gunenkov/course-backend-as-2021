﻿namespace Shared.Services.InputOutput.Abstractions
{
    public abstract class InputOutputService
    {
        public delegate void LogMessage(string message);

        /// <summary>
        ///     Метод выводит строку в текущий поток вывода
        /// </summary>
        /// <param name="str">Строка, которую необходимо вывести</param>
        public abstract void WriteLine(string str);

        /// <summary>
        ///     Метод считывает строку из текущего потока ввода
        /// </summary>
        /// <returns>Считанная строка</returns>
        public abstract string ReadLine();

        public abstract event LogMessage ClientInput;
    }
}