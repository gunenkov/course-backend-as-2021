﻿using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Shared.Services.Serialization.Abstractions;

namespace Shared.Services.Serialization
{
    public class JsonSerializationService : ISerializationService
    {
        public static JsonSerializerOptions JsonSerializerOptions = new JsonSerializerOptions();

        public async Task SaveObjectToFile(object obj, string filename)
        {
            await using var stream = new FileStream(filename, FileMode.OpenOrCreate);
            {
                await JsonSerializer.SerializeAsync(stream, obj, JsonSerializerOptions);
            }
        }

        public async Task<T> LoadObjectFromFile<T>(string filename)
        {
            if (!File.Exists(filename)) await File.WriteAllTextAsync(filename, "[]");

            await using var stream = new FileStream(filename, FileMode.OpenOrCreate);
            {
                return await JsonSerializer.DeserializeAsync<T>(stream, JsonSerializerOptions);
            }
        }

        public string SearializeObject<T>(T obj)
        {
            return JsonSerializer.Serialize(obj, JsonSerializerOptions);
        }

        public T DesearializeObject<T>(string jsonString)
        {
            return JsonSerializer.Deserialize<T>(jsonString, JsonSerializerOptions);
        }
    }
}