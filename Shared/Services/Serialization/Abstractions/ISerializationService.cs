﻿using System.Threading.Tasks;

namespace Shared.Services.Serialization.Abstractions
{
    public interface ISerializationService
    {
        Task SaveObjectToFile(object obj, string filename);
        Task<T> LoadObjectFromFile<T>(string filename);
        string SearializeObject<T>(T obj);
        T DesearializeObject<T>(string jsonString);
    }
}