﻿using System;
using System.Threading.Tasks;
using Shared.Services.Serialization.Abstractions;

namespace Shared.Services.Serialization
{
    public class ExcelSerializationService : ISerializationService
    {
        public async Task SaveObjectToFile(object obj, string filename)
        {
            throw new NotImplementedException();
        }

        public Task<T> LoadObjectFromFile<T>(string filename)
        {
            throw new NotImplementedException();
        }

        public string SearializeObject<T>(T obj)
        {
            throw new NotImplementedException();
        }

        public T DesearializeObject<T>(string jsonString)
        {
            throw new NotImplementedException();
        }
    }
}