﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Data.Models.Abstractions;

namespace Shared.Data.ORM
{
    public static class DatabaseExtensions
    {
        public static async Task<IReadOnlyCollection<TItem>> QueryMultipleItems<TItem>(this IDbConnection connection,
            string sqlQuery) where TItem : Entity
        {
            var results = new List<TItem>();
            var type = typeof(TItem);
            var command = new SqlCommand(sqlQuery, connection as SqlConnection) {CommandText = sqlQuery};
            await using (var reader = await command.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    var item = (TItem) Activator.CreateInstance(type);

                    foreach (var property in type.GetProperties().ToList())
                        property.SetValue(item, reader[property.Name]);

                    results.Add(item);
                }
            }

            return results.AsReadOnly();
        }

        public static async Task InsertItem<TItem>(this IDbConnection connection, TItem item) where TItem : Entity
        {
            var type = typeof(TItem);
            var properties = type.GetProperties().Where(property => property.Name != "Id").ToList();
            var commaSeparatedValuesBuilder = new StringBuilder();
            foreach (var property in properties)
            {
                if (property.PropertyType.Name == "String")
                {
                    commaSeparatedValuesBuilder.Append('\'');
                    commaSeparatedValuesBuilder.Append(property.GetValue(item));
                    commaSeparatedValuesBuilder.Append('\'');
                }
                else
                {
                    commaSeparatedValuesBuilder.Append(property.GetValue(item));
                }

                commaSeparatedValuesBuilder.Append(", ");
            }

            var commaSeparatedPropertyNamesBuilder = new StringBuilder();
            foreach (var property in properties)
            {
                commaSeparatedPropertyNamesBuilder.Append(property.Name);
                commaSeparatedPropertyNamesBuilder.Append(", ");
            }

            var commaSeparatedValues = commaSeparatedValuesBuilder.ToString();
            commaSeparatedValues = commaSeparatedValues.Remove(commaSeparatedValues.Length - 2);

            var commaSeparatedPropertyNames = commaSeparatedPropertyNamesBuilder.ToString();
            commaSeparatedPropertyNames = commaSeparatedPropertyNames.Remove(commaSeparatedPropertyNames.Length - 2);

            var command = new SqlCommand($"insert into {type.Name} ({commaSeparatedPropertyNames})" +
                                         $" values ({commaSeparatedValues})", connection as SqlConnection);
            await command.ExecuteScalarAsync();
        }

        public static async Task UpdateItem<TItem>(this IDbConnection connection, TItem item) where TItem : Entity
        {
            var type = typeof(TItem);
            var properties = type.GetProperties().Where(property => property.Name != "Id").ToList();

            var commaSeparatedValuesBuilder = new StringBuilder();
            foreach (var property in properties)
            {
                if (property.PropertyType.Name == "String")
                {
                    commaSeparatedValuesBuilder.Append(property.Name);
                    commaSeparatedValuesBuilder.Append("=");
                    commaSeparatedValuesBuilder.Append('\'');
                    commaSeparatedValuesBuilder.Append(property.GetValue(item));
                    commaSeparatedValuesBuilder.Append('\'');
                }
                else
                {
                    commaSeparatedValuesBuilder.Append(property.Name);
                    commaSeparatedValuesBuilder.Append("=");
                    commaSeparatedValuesBuilder.Append(property.GetValue(item));
                }

                commaSeparatedValuesBuilder.Append(", ");
            }

            var commaSeparatedValues = commaSeparatedValuesBuilder.ToString();
            commaSeparatedValues = commaSeparatedValues.Remove(commaSeparatedValues.Length - 2);

            var command = new SqlCommand($"update {type.Name} set {commaSeparatedValues} where Id = {item.Id}",
                connection as SqlConnection);
            await command.ExecuteScalarAsync();
        }

        public static async Task DeleteItem<TItem>(this IDbConnection connection, int id) where TItem : Entity
        {
            var type = typeof(TItem);

            var command = new SqlCommand($"delete from {type.Name} where Id = {id}",
                connection as SqlConnection);
            await command.ExecuteScalarAsync();
        }
    }
}