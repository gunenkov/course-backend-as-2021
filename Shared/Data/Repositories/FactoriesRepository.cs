﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shared.Data.Models;
using Shared.Data.Repositories.Abstractions;
using Shared.Data.UnitsOfWork;

namespace Shared.Data.Repositories
{
    public class FactoriesRepository : Repository<Factory>, IFactoriesRepository
    {
        public FactoriesRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public async Task<Factory> GetFactoryWithUnitsAndTanks(int factoryId)
        {
            return await databaseContext.Factories.Include(factory => factory.Units).ThenInclude(unit => unit.Tanks)
                .FirstOrDefaultAsync(factory => factory.Id == factoryId);
        }
    }
}