﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Shared.Data.Models;
using Shared.Data.Repositories.Abstractions;
using Shared.Data.UnitsOfWork;

namespace Shared.Data.Repositories
{
    public class TanksRepository : Repository<Tank>, ITanksRepository
    {
        public TanksRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public int GetTotalTanksVolume()
        {
            return GetAll().Sum(tank => tank.Volume);
        }

        public async Task<int> RandomUpdateVolume(int tankId)
        {
            var tank = await GetById(tankId);
            var random = new Random();

            var currentVolume = tank.Volume;
            var maxDeviation = Convert.ToInt32(tank.Volume * 0.1);
            tank.Volume = random.Next(currentVolume - maxDeviation, currentVolume + maxDeviation);

            await databaseContext.SaveChangesAsync();
            return tank.Volume;
        }
    }
}