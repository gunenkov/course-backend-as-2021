﻿using System.Linq;
using System.Threading.Tasks;
using Shared.Data.Models.Abstractions;

namespace Shared.Data.Repositories.Abstractions
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        IQueryable<TEntity> GetAll();

        // IQueryable<TEntity> GetByPredicate(Func<TEntity, bool> predicate);
        Task<TEntity> GetById(int id);
        Task Add(TEntity entity);
        Task Update(object newEntity, TEntity oldEntity);
        Task Delete(int id);
    }
}