﻿using System.Threading.Tasks;
using Shared.Data.Models;

namespace Shared.Data.Repositories.Abstractions
{
    public interface ITanksRepository : IRepository<Tank>
    {
        int GetTotalTanksVolume();
        Task<int> RandomUpdateVolume(int tankId);
    }
}