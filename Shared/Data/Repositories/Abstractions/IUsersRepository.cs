﻿using System.Threading.Tasks;
using Shared.Data.Models.Identity;

namespace Shared.Data.Repositories.Abstractions
{
    public interface IUsersRepository : IRepository<User>
    {
        Task<User> GetByUserName(string userName);
        Task<User> GetByUserNameWithRole(string userName);
        Task UpdatePassword(User user, string newPassword);
    }
}