﻿using System.Threading.Tasks;
using Shared.Data.Models;

namespace Shared.Data.Repositories.Abstractions
{
    public interface IFactoriesRepository : IRepository<Factory>
    {
        Task<Factory> GetFactoryWithUnitsAndTanks(int factoryId);
    }
}