﻿using System.Threading.Tasks;
using Shared.Data.Models;

namespace Shared.Data.Repositories.Abstractions
{
    public interface IUnitsRepository : IRepository<Unit>
    {
        Task<Unit> GetUnitWithTanks(int unitId);
    }
}