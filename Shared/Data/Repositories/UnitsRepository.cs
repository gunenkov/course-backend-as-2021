﻿using System.Threading.Tasks;
using Shared.Data.Models;
using Shared.Data.Repositories.Abstractions;
using Shared.Data.UnitsOfWork;

namespace Shared.Data.Repositories
{
    public class UnitsRepository : Repository<Unit>, IUnitsRepository
    {
        public UnitsRepository(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public async Task<Unit> GetUnitWithTanks(int unitId)
        {
            var unit = await databaseContext.Units.FindAsync(unitId);
            await databaseContext.Entry(unit).Collection(u => u.Tanks).LoadAsync();
            return unit;
        }
    }
}