﻿using System.Linq;
using System.Threading.Tasks;
using Shared.Data.Models.Abstractions;
using Shared.Data.Repositories.Abstractions;
using Shared.Data.UnitsOfWork;

namespace Shared.Data.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly DatabaseContext databaseContext;

        protected Repository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            return databaseContext.Set<TEntity>();
        }

        public async Task<TEntity> GetById(int id)
        {
            return await databaseContext.Set<TEntity>().FindAsync(id);
        }

        public async Task Add(TEntity entity)
        {
            await databaseContext.Set<TEntity>().AddAsync(entity);
            await databaseContext.SaveChangesAsync();
        }

        public async Task Update(object newEntity, TEntity oldEntity)
        {
            databaseContext.Entry(oldEntity).CurrentValues.SetValues(newEntity);
            await databaseContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            databaseContext.Set<TEntity>().Remove(await GetById(id));
            await databaseContext.SaveChangesAsync();
        }
    }
}