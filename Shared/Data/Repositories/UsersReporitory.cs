﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shared.Data.Models.Identity;
using Shared.Data.Repositories.Abstractions;
using Shared.Data.UnitsOfWork;

namespace Shared.Data.Repositories
{
    public class UsersReporitory : Repository<User>, IUsersRepository
    {
        public UsersReporitory(DatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public async Task<User> GetByUserName(string userName)
        {
            return await databaseContext.Users.FirstOrDefaultAsync(u => u.UserName == userName);
        }

        public async Task<User> GetByUserNameWithRole(string userName)
        {
            var user = await databaseContext.Users.FirstOrDefaultAsync(u => u.UserName == userName);
            if (user != null) await databaseContext.Entry(user).Reference(u => u.Role).LoadAsync();

            return user;
        }

        public async Task UpdatePassword(User user, string newPassword)
        {
            user.Password = newPassword;
            await databaseContext.SaveChangesAsync();
        }
    }
}