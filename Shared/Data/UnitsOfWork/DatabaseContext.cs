﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Shared.Data.Models;
using Shared.Data.Models.Identity;

namespace Shared.Data.UnitsOfWork
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Factory> Factories { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Tank> Tanks { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(new Role {Id = 1, Name = "Administrator"});

            var user = new User();
            var passwordHash = new PasswordHasher<User>().HashPassword(user, "pwd123");
            modelBuilder.Entity<User>().HasData(new User
                {Id = 1, UserName = "admin", Password = passwordHash, RoleId = 1});
        }
    }
}