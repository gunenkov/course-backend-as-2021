﻿using System.Data;
using Shared.Data.Repositories.Abstractions;

namespace Shared.Data.UnitsOfWork.Abstractions
{
    public interface IDatabaseContext
    {
        IFactoriesRepository FactoriesRepository { get; set; }
        IUnitsRepository UnitsRepository { get; set; }
        ITanksRepository TanksRepository { get; set; }
        IDbConnection GetDatabaseConnection();
        IDatabaseContext InitializeRepositories();
        IDatabaseContext OpenDatabaseConnection();
        IDatabaseContext UseDatabaseConnection(string connectionString);
    }
}