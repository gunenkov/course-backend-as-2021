﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Shared.Data.Models.Abstractions;

namespace Shared.Data.Models
{
    /// <summary>
    ///     Установка
    /// </summary>
    public class Unit : Entity
    {
        public string Name { get; set; }
        public int FactoryId { get; set; }
        [JsonIgnore] public Factory Factory { get; set; }
        public IList<Tank> Tanks { get; set; }
    }
}