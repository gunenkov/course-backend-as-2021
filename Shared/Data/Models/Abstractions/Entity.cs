﻿namespace Shared.Data.Models.Abstractions
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}