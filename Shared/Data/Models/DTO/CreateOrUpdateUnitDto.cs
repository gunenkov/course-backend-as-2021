﻿namespace Shared.Data.Models.DTO
{
    public class CreateOrUpdateUnitDto
    {
        public string Name { get; set; }
    }
}