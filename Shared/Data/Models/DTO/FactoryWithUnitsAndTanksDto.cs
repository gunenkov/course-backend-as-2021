﻿using System.Collections.Generic;

namespace Shared.Data.Models.DTO
{
    public class FactoryWithUnitsAndTanksDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IList<UnitWithTanksDto> Units { get; set; }
    }
}