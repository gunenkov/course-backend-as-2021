﻿namespace Shared.Data.Models.DTO
{
    public class TankDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MaxVolume { get; set; }
        public int Volume { get; set; }
    }
}