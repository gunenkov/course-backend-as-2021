﻿namespace Shared.Data.Models.DTO
{
    public class UserUpdatePasswordDto
    {
        public string UserName { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}