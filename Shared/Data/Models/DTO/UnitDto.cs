﻿namespace Shared.Data.Models.DTO
{
    public class UnitDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}