﻿using System.Collections.Generic;

namespace Shared.Data.Models.DTO
{
    public class UnitWithTanksDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<Tank> Tanks { get; set; }
    }
}