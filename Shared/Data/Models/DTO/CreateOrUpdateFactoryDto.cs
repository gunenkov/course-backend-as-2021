﻿namespace Shared.Data.Models.DTO
{
    public class CreateOrUpdateFactoryDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}