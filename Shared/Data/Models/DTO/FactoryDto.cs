﻿namespace Shared.Data.Models.DTO
{
    public class FactoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}