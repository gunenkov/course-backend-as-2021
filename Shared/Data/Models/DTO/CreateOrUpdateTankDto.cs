﻿namespace Shared.Data.Models.DTO
{
    public class CreateOrUpdateTankDto
    {
        public string Name { get; set; }
        public int MaxVolume { get; set; }
        public int Volume { get; set; }
    }
}