﻿using Shared.Data.Models.Abstractions;

namespace Shared.Data.Models.Identity
{
    public class Role : Entity
    {
        public string Name { get; set; }
    }
}