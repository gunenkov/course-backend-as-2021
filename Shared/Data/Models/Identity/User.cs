﻿using Shared.Data.Models.Abstractions;

namespace Shared.Data.Models.Identity
{
    public class User : Entity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}