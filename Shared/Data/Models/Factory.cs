﻿using System.Collections.Generic;
using Shared.Data.Models.Abstractions;

namespace Shared.Data.Models
{
    /// <summary>
    ///     Завод
    /// </summary>
    public class Factory : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IList<Unit> Units { get; set; } = new List<Unit>();
    }
}