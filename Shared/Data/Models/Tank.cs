﻿using System.Text.Json.Serialization;
using Shared.Data.Models.Abstractions;

namespace Shared.Data.Models
{
    /// <summary>
    ///     Резервуар
    /// </summary>
    public class Tank : Entity
    {
        public string Name { get; set; }
        public int Volume { get; set; }
        public int MaxVolume { get; set; }
        public int UnitId { get; set; }
        [JsonIgnore] public Unit Unit { get; set; }
    }
}