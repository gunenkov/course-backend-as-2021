﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Migrations
{
    public partial class IdentityWithPasswordHashing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                "Users",
                "Id",
                1,
                "Password",
                "AQAAAAEAACcQAAAAEE/SfYOBxnJR2XLUWHnRuwkbu+tYER6kJaQBTx8waGsjSnjEdE9G79G19qqcSxjtQw==");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                "Users",
                "Id",
                1,
                "Password",
                "pwd123");
        }
    }
}