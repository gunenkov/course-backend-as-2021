﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Shared.Migrations
{
    public partial class InitialMigrationPostgre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Factories",
                table => new
                {
                    Id = table.Column<int>("integer")
                        .Annotation("Npgsql:ValueGenerationStrategy",
                            NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>("text", nullable: true),
                    Description = table.Column<string>("text", nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Factories", x => x.Id); });

            migrationBuilder.CreateTable(
                "Units",
                table => new
                {
                    Id = table.Column<int>("integer")
                        .Annotation("Npgsql:ValueGenerationStrategy",
                            NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>("text", nullable: true),
                    FactoryId = table.Column<int>("integer")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                    table.ForeignKey(
                        "FK_Units_Factories_FactoryId",
                        x => x.FactoryId,
                        "Factories",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "Tanks",
                table => new
                {
                    Id = table.Column<int>("integer")
                        .Annotation("Npgsql:ValueGenerationStrategy",
                            NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>("text", nullable: true),
                    Volume = table.Column<int>("integer"),
                    MaxVolume = table.Column<int>("integer"),
                    UnitId = table.Column<int>("integer")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tanks", x => x.Id);
                    table.ForeignKey(
                        "FK_Tanks_Units_UnitId",
                        x => x.UnitId,
                        "Units",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                "IX_Tanks_UnitId",
                "Tanks",
                "UnitId");

            migrationBuilder.CreateIndex(
                "IX_Units_FactoryId",
                "Units",
                "FactoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "Tanks");

            migrationBuilder.DropTable(
                "Units");

            migrationBuilder.DropTable(
                "Factories");
        }
    }
}