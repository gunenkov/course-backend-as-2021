﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Shared.Migrations
{
    public partial class IdentityModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Roles",
                table => new
                {
                    Id = table.Column<int>("integer")
                        .Annotation("Npgsql:ValueGenerationStrategy",
                            NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>("text", nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Roles", x => x.Id); });

            migrationBuilder.CreateTable(
                "Users",
                table => new
                {
                    Id = table.Column<int>("integer")
                        .Annotation("Npgsql:ValueGenerationStrategy",
                            NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>("text", nullable: true),
                    Password = table.Column<string>("text", nullable: true),
                    RoleId = table.Column<int>("integer")
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.Id); });

            migrationBuilder.InsertData(
                "Roles",
                new[] {"Id", "Name"},
                new object[] {1, "Administrator"});

            migrationBuilder.InsertData(
                "Users",
                new[] {"Id", "Password", "RoleId", "UserName"},
                new object[] {1, "pwd123", 1, "admin"});
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "Roles");

            migrationBuilder.DropTable(
                "Users");
        }
    }
}