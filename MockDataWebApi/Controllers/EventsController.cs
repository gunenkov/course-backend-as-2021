﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MockDataWebApi.Models;
using Shared.Services.Serialization;

namespace MockDataWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Route("keys")]
        public async Task<ActionResult<IList<int>>> GetEventIdsByUnit([FromQuery] int unitId = 1,
            [FromQuery] int take = 3, [FromQuery] int skip = 3)
        {
            if (take > 100) return BadRequest("Выдавать можно максимум 100 событий");
            var events = await new JsonSerializationService().LoadObjectFromFile<IList<Event>>("events.json");
            return Ok(events.Where(e => e.UnitId == unitId).Skip(skip).Take(take).Select(e => e.Id));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IList<Event>>> GetEventsByIds(IEnumerable<int> ids)
        {
            var events = await new JsonSerializationService().LoadObjectFromFile<IList<Event>>("events.json");
            return Ok(events.Where(e => ids.Contains(e.Id)));
        }
    }
}