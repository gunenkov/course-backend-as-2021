﻿using System.Text.Json.Serialization;

namespace MockDataWebApi.Models
{
    public class Operator
    {
        [JsonPropertyName("id")] public int Id { get; set; }

        [JsonPropertyName("name")] public string Name { get; set; }
    }
}