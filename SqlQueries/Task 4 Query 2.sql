/* ��������� �������� Volume � MaxVolume, � ����� ���������� ����������� �� ������ ���������,
� ������� ����� ���������, � ����� ����� � �������� ������, � �������� ��������� ��������� */

USE CourseAS

SELECT Unit.Name AS '���������', SUM(Tank.Volume) AS '���. �����',
SUM(Tank.MaxVolume) AS '���. ����. �����', COUNT(Tank.Id) AS '���������� �����������',
Factory.Name AS '�����', Factory.Description AS '�������� ������'
FROM Unit
LEFT JOIN Tank ON Tank.UnitId = Unit.Id LEFT JOIN Factory ON Factory.Id = Unit.FactoryId
GROUP BY Unit.Name, Factory.Name, Factory.Description