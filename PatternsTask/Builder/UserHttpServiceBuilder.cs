﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using PatternsTask.Builder.Abstractions;

namespace PatternsTask.Builder
{
    public class UserHttpServiceBuilder : IUserHttpServiceBuilder
    {
        private readonly Mock<IUserHttpService> mock = new Mock<IUserHttpService>();

        public IUserHttpServiceBuilder WithGetAllUsersAsync(IList<IUser> users)
        {
            mock.Setup(m => m.GetAllUsers()).Returns(Task.FromResult(users));
            return this;
        }

        public IUserHttpServiceBuilder WithGetAllBrigadesAsync(IList<BrigadeDto> brigades)
        {
            mock.Setup(m => m.GetAllBrigades()).Returns(Task.FromResult(brigades));
            return this;
        }

        public IUserHttpServiceBuilder WithGetUserById(IUser user)
        {
            mock.Setup(m => m.GetUserById(user.Id)).Returns(Task.FromResult(user));
            return this;
        }

        public IUserHttpServiceBuilder WithGetBrigadeById(BrigadeDto brigade)
        {
            mock.Setup(m => m.GetBrigadeById(brigade.Id)).Returns(Task.FromResult(brigade));
            return this;
        }

        public IUserHttpService Build()
        {
            return mock.Object;
        }
    }
}