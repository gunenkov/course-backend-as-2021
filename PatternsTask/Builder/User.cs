﻿using PatternsTask.Builder.Abstractions;

namespace PatternsTask.Builder
{
    public class User : IUser
    {
        public int Id { get; set; }
    }
}