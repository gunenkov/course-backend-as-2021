﻿namespace PatternsTask.Builder.Abstractions
{
    public interface IUser
    {
        int Id { get; set; }
    }
}