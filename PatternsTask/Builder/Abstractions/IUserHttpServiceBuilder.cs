﻿using System.Collections.Generic;

namespace PatternsTask.Builder.Abstractions
{
    public interface IUserHttpServiceBuilder
    {
        IUserHttpServiceBuilder WithGetAllUsersAsync(IList<IUser> users);
        IUserHttpServiceBuilder WithGetAllBrigadesAsync(IList<BrigadeDto> brigades);
        IUserHttpServiceBuilder WithGetUserById(IUser user);
        IUserHttpServiceBuilder WithGetBrigadeById(BrigadeDto brigade);
        IUserHttpService Build();
    }
}