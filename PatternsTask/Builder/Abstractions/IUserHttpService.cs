﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PatternsTask.Builder.Abstractions
{
    public interface IUserHttpService
    {
        Task<IList<IUser>> GetAllUsers();
        Task<IList<BrigadeDto>> GetAllBrigades();
        Task<IUser> GetUserById(int id);
        Task<BrigadeDto> GetBrigadeById(int id);
    }
}