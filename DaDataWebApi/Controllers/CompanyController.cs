﻿using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DaDataShared.Models;
using DaDataWebApi.Hubs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;

namespace DaDataWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly HttpClient client;
        private readonly IHttpClientFactory clientFactory;
        private readonly IHubContext<CompanyHub> companyHubContext;
        private readonly IConfiguration configuration;
        private readonly HttpClient httpClient;

        public CompanyController(IConfiguration configuration,
            IHubContext<CompanyHub> companyHubContext, IHttpClientFactory clientFactory)
        {
            this.configuration = configuration;
            this.companyHubContext = companyHubContext;
            this.clientFactory = clientFactory;
            httpClient = clientFactory.CreateClient("Client");
        }

        [Route("{inn}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetCompany(string inn)
        {
            if (string.IsNullOrWhiteSpace(inn)) return BadRequest("ИНН не может быть пустым");

            var requestBody = new {query = inn, count = 1};
            var content = new StringContent(JsonSerializer.Serialize(requestBody), Encoding.UTF8, "application/json");

            var response = await httpClient.PostAsync(configuration["RequestUrl"],
                content);
            var responseContent =
                JsonSerializer.Deserialize<ResponseContent>(await response.Content.ReadAsStringAsync());

            if (responseContent.Suggestions == null) return NotFound("Организаций не найдено");

            var company = responseContent.Suggestions.FirstOrDefault();
            if (company == null) return NotFound("Организаций не найдено");

            await companyHubContext.Clients.All.SendAsync("GetCompanyName", company.Value);
            return Ok(company.Value);
        }
    }
}