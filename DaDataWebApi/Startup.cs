using System;
using System.Net.Http;
using DaDataWebApi.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace DaDataWebApi
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IHostEnvironment environment)
        {
            var configurationBuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", true, true);
            configuration = configurationBuilder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddHttpClient("Client", client => client.DefaultRequestHeaders
                .Add("Authorization", $"Token {configuration["ApiToken"]}"));

            services.AddSwaggerGen(options => options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "CourseAS DaData Web API",
                Description = "Api developed during Lesson 5 of AS course 2021",
                Contact = new OpenApiContact
                {
                    Name = "Mikhail Gunenkov",
                    Url = new Uri("https://vk.com/m_gunenkov")
                }
            }));

            services.AddSignalR();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, HttpClient client)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "CourseAS API"));

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<CompanyHub>("/companyhub");
            });
        }
    }
}