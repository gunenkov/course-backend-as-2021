﻿namespace Lesson1
{
    internal class Program
    {
        /* private static IConfiguration configuration;

        private static void ConfigureServices()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            configuration = builder.Build();

            var services = new ServiceCollection();
            services.AddDbContextPool<DatabaseContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
        }

        private static async Task Main(string[] args, DatabaseContext db)
        {
            #region Инициализация базы данных

            ConfigureServices();

            InputOutputService inputOutputLayer = new ConsoleService();
            inputOutputLayer.ClientInput += (s) => inputOutputLayer.WriteLine(s);

            #endregion

            #region Работа с базой данных

            // выводим количество резервуаров в таблице БД
            inputOutputLayer.WriteLine($"Количество резервуаров: {(await db.TanksRepository.GetAll()).Count}\n");

            // выводим на печать все резервуары
            await WriteTanksInfo(inputOutputLayer, db, await db.TanksRepository.GetAll());

            // выводим на печать общую сумму загрузки всех резервуаров
            inputOutputLayer.WriteLine($"Общий объем резервуаров: {await db.TanksRepository.GetTotalTanksVolume()}\n");

            // ищем резервуар по имени; выводим его максимальный объем (поле найденного объекта)
            inputOutputLayer.WriteLine("Введите имя резервуара для поиска:");
            try
            {
                // method chain
                var input = inputOutputLayer.ReadLine();
                IEnumerable<Tank> tanks = await db.TanksRepository.GetByPredicate(tank =>
                    tank.Name.ToLower().Contains(input.ToLower()));

                // query expression
                tanks = from tank in await db.TanksRepository.GetAll()
                    where tank.Name.ToLower().Contains(input.ToLower())
                    select tank;

                await WriteTanksInfo(inputOutputLayer, db, tanks);
            }
            catch (Exception ex)
            {
                inputOutputLayer.WriteLine(ex.Message);
            }

            #endregion
        }

        /// <summary>
        /// Вывод на печать информации о резервуарах
        /// </summary>
        /// <param name="inputOutputLayer">Слой ввода-выода, используемый для вывода информации</param>
        /// <param name="db">Объект базы данных</param>
        /// <param name="tanks">Резервуары, о которых требуется вывести информацию</param>
        public static async Task WriteTanksInfo(InputOutputService inputOutputLayer, IDatabaseContext db,
            IEnumerable<Tank> tanks)
        {
            foreach (var tank in tanks)
            {
                if (tank == null)
                {
                    inputOutputLayer.WriteLine("Нет данных");
                    return;
                }

                Unit unit;
                Factory factory;

                try
                {
                    unit = await db.UnitsRepository.GetById(tank.UnitId);
                }
                catch
                {
                    unit = new Unit {Name = "None"};
                }

                try
                {
                    factory = await db.FactoriesRepository.GetById(unit.FactoryId);
                }
                catch
                {
                    factory = new Factory {Name = "None"};
                }

                inputOutputLayer.WriteLine($"Резервуар c ID={tank.Id}: \"{tank.Name}\"\n" +
                                           $"Текущий объем: {tank.Volume}\n" +
                                           $"Максимальный объем: {tank.MaxVolume}\n" +
                                           $"Принадлежит установке: {unit.Name}\n" +
                                           $"Принадлежит заводу: {factory.Name}\n");
            }
        }*/
    }
}