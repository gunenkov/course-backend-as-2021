﻿using System.Net.Http;

namespace CollectionsTask.Http
{
    public class SingletonHttpClient
    {
        private static HttpClient instance;

        private SingletonHttpClient()
        {
        }

        public static HttpClient GetInstance()
        {
            return instance ??= new HttpClient();
        }
    }
}