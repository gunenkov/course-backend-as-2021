﻿using System.Text.Json;
using System.Threading.Tasks;
using CollectionsTask.Http;
using Shared.Services.Serialization;

namespace CollectionsTask.Services
{
    public class DownloadService : IDownloadService
    {
        public async Task<T> DownloadObject<T>(string url)
        {
            JsonSerializationService.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            return new JsonSerializationService()
                .DesearializeObject<T>(await SingletonHttpClient.GetInstance().GetStringAsync(url));
        }
    }
}