﻿using System.Threading.Tasks;

namespace CollectionsTask.Services
{
    public interface IDownloadService
    {
        Task<T> DownloadObject<T>(string url);
    }
}