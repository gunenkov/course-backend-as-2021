﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CollectionsTask.Models;
using CollectionsTask.Services;

namespace CollectionsTask
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            IDownloadService downloadService = new DownloadService();
            var posts = await downloadService.DownloadObject<List<Post>>("https://jsonplaceholder.typicode.com/posts");
        }
    }
}