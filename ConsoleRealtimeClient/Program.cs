﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace ConsoleRealtimeClient
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem(async f =>
            {
                var hubConnection = new HubConnectionBuilder().WithUrl("https://localhost:44337/string").Build();
                hubConnection.StartAsync().ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine(
                            $"Возникла ошибка при подключении к хабу: {task.Exception.GetBaseException()}");
                    }
                    else
                    {
                        Console.WriteLine("Успешное подключение к хабу");
                        Console.WriteLine("Нажмите любую клавишу, чтобы выйти...");
                        hubConnection.On<string>("StringChannel", Console.WriteLine);

                        while (true)
                        {
                            var userInput = Console.ReadLine();

                            if (string.IsNullOrEmpty(userInput)) break;
                        }
                    }
                }).Wait();
            });

            var hubConnection = new HubConnectionBuilder().WithUrl("https://localhost:44337/chart").Build();
            hubConnection.StartAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine(
                        $"Возникла ошибка при подключении к хабу: {task.Exception.GetBaseException()}");
                }
                else
                {
                    Console.WriteLine("Успешное подключение к хабу");
                    Console.WriteLine("Нажмите любую клавишу, чтобы выйти...");
                    hubConnection.On<string>("SimpleChartChannel", Console.WriteLine);

                    while (true)
                    {
                        var userInput = Console.ReadLine();

                        if (string.IsNullOrEmpty(userInput)) break;
                    }
                }
            }).Wait();

            await hubConnection.StopAsync();
            await hubConnection.DisposeAsync();
        }
    }
}