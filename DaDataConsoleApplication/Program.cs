﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;

namespace DaDataConsoleApplication
{
    internal class Program
    {
        private static IConfiguration configuration;
        private static HubConnection hubConnection;

        private static void ConfigureServices()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            configuration = builder.Build();
        }

        private static async Task Main(string[] args)
        {
            ConfigureServices();

            hubConnection = new HubConnectionBuilder().WithUrl(configuration["HubUrl"]).Build();
            hubConnection.StartAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine($"Возникла ошибка при подключении к хабу: {task.Exception.GetBaseException()}");
                }
                else
                {
                    Console.WriteLine("Успешное подключение к хабу");
                    Console.WriteLine("Нажмите любую клавишу, чтобы выйти...");
                    hubConnection.On<string>("GetCompanyName", Console.WriteLine);

                    while (true)
                    {
                        var userInput = Console.ReadLine();

                        if (string.IsNullOrEmpty(userInput)) break;
                    }
                }
            }).Wait();

            await hubConnection.StopAsync();
            await hubConnection.DisposeAsync();
        }
    }
}