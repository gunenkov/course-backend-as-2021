﻿using System;
using System.Windows;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TanksMonitorWpf.Extensions;
using TanksMonitorWpf.HostedServices;
using TanksMonitorWpf.Services;
using TanksMonitorWpf.Services.Abstractions;
using TanksMonitorWpf.ViewModels;
using TanksMonitorWpf.Views;

namespace TanksMonitorWpf
{
    public partial class App : Application
    {
        private readonly IHost host;

        public App()
        {
            host = new HostBuilder()
                .ConfigureAppConfiguration((context, builder) => builder.AddJsonFile("appsettings.json"))
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton<MainWindowViewModel>();
                    services.AddHostedService<AuthorizationCheckingHostedService>();
                    services.AddJsonSerizalizationService();
                    services.AddSingleton<IUserService, UserService>();
                    services.AddHttpClient("Client");
                    services.AddSingleton<ITanksService, TanksService>();
                }).Build();
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            var viewModel = host.Services.GetRequiredService<MainWindowViewModel>();
            var loginViewModel = new LoginViewModel(host.Services.GetRequiredService<IUserService>());
            var tanksListViewModel = new TanksListViewModel(host.Services.GetRequiredService<ITanksService>());

            var mainWindow = new MainWindow {DataContext = viewModel};

            viewModel.LoginViewModel = loginViewModel;
            viewModel.LoginView = new LoginView {DataContext = loginViewModel};

            viewModel.TanksListViewModel = tanksListViewModel;
            viewModel.TanksListView = new TanksListView {DataContext = tanksListViewModel};

            viewModel.InitMainWindow(mainWindow);
            await host.StartAsync();
        }

        protected override async void OnExit(ExitEventArgs e)
        {
            await host.StopAsync(TimeSpan.FromSeconds(3));
        }
    }
}