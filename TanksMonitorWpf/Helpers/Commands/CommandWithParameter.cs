﻿using System;
using System.Windows.Input;

namespace TanksMonitorWpf.Helpers.Commands
{
    public class CommandWithParameter : ICommand
    {
        private readonly Action<object> callBack;

        public CommandWithParameter(Action<object> callBack)
        {
            this.callBack = callBack;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            callBack(parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}