﻿using System;
using System.Threading.Tasks;
using TanksMonitorWpf.Helpers.Commands.Asynchronous.Abstractions;

namespace TanksMonitorWpf.Helpers.Commands.Asynchronous
{
    public class AsyncCommand : BaseAsyncCommand
    {
        private readonly Func<Task> _callback;

        public AsyncCommand(Func<Task> callback)
        {
            _callback = callback;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            await _callback();
        }
    }
}