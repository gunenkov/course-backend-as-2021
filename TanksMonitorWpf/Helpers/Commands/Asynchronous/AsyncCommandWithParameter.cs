﻿using System;
using System.Threading.Tasks;
using TanksMonitorWpf.Helpers.Commands.Asynchronous.Abstractions;

namespace TanksMonitorWpf.Helpers.Commands.Asynchronous
{
    public class AsyncCommandWithParameter : BaseAsyncCommand
    {
        private readonly Func<object, Task> _callback;

        public AsyncCommandWithParameter(Func<object, Task> callback)
        {
            _callback = callback;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            await _callback(parameter);
        }
    }
}