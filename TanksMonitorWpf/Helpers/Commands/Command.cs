﻿using System;
using System.Windows.Input;

namespace TanksMonitorWpf.Helpers.Commands
{
    public class Command : ICommand
    {
        private readonly Action callBack;

        public Command(Action callBack)
        {
            this.callBack = callBack;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            callBack();
        }

        public event EventHandler CanExecuteChanged;
    }
}