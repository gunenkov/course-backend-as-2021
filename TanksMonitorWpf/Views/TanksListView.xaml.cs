﻿using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace TanksMonitorWpf.Views
{
    public partial class TanksListView : UserControl
    {
        public TanksListView()
        {
            InitializeComponent();
        }

        private void PreviewNumericInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9]+").IsMatch(e.Text);
        }
    }
}