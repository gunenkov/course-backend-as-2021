﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using TanksMonitorWpf.Services.Abstractions;
using TanksMonitorWpf.ViewModels;

namespace TanksMonitorWpf.HostedServices
{
    public class AuthorizationCheckingHostedService : BackgroundService
    {
        private readonly MainWindowViewModel mainWindowViewModel;
        private readonly IUserService userService;

        public AuthorizationCheckingHostedService(IUserService userService, MainWindowViewModel mainWindowViewModel)
        {
            this.userService = userService;
            this.mainWindowViewModel = mainWindowViewModel;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (userService.UserName != null && !await userService.IsAuthorized())
                    mainWindowViewModel.NavigateToLoginView();

                await Task.Delay(10000, stoppingToken);
            }
        }
    }
}