﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Shared.Data.Models;
using TanksMonitorWpf.Helpers.Commands.Asynchronous;
using TanksMonitorWpf.Services.Abstractions;
using TanksMonitorWpf.ViewModels.Abstractions;

namespace TanksMonitorWpf.ViewModels
{
    public class TanksListViewModel : BaseViewModel
    {
        private ObservableCollection<Tank> tanks;

        public TanksListViewModel(ITanksService tanksService)
        {
            GetDataCommand = new AsyncCommand(async () =>
                Tanks = new ObservableCollection<Tank>(await tanksService.GetData()));

            DeleteCommand = new AsyncCommandWithParameter(async item =>
            {
                var deleted = await tanksService.DeleteItem(item as Tank);
                if (deleted) Tanks.Remove(item as Tank);
            });

            UpdateCommand =
                new AsyncCommandWithParameter(async item => await tanksService.UpdateItem(item as Tank));
        }

        public ICommand GetDataCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand UpdateCommand { get; set; }

        public ObservableCollection<Tank> Tanks
        {
            get => tanks;
            set
            {
                tanks = value;
                OnPropertyChanged(nameof(Tanks));
            }
        }
    }
}