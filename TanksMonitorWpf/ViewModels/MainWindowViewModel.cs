﻿using System.Windows;
using System.Windows.Input;
using TanksMonitorWpf.Helpers.Commands;
using TanksMonitorWpf.ViewModels.Abstractions;
using TanksMonitorWpf.Views;

namespace TanksMonitorWpf.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        private string userName;

        public MainWindowViewModel()
        {
            ExitCommand = new Command(() => Application.Current.Shutdown(0));
        }

        public ICommand ExitCommand { get; set; }

        public string UserName
        {
            get => userName;
            set
            {
                userName = value;
                OnPropertyChanged(nameof(UserName));
            }
        }

        public MainWindow MainWindow { get; set; }
        public LoginView LoginView { get; set; }
        public LoginViewModel LoginViewModel { get; set; }
        public TanksListView TanksListView { get; set; }
        public TanksListViewModel TanksListViewModel { get; set; }

        public void InitMainWindow(MainWindow mainWindow)
        {
            MainWindow = mainWindow;
            MainWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            MainWindow.Show();
            MainWindow.FrameBody.NavigationService.Navigate(LoginView);
        }

        public void NavigateToTanksListView()
        {
            MainWindow.FrameBody.NavigationService.Navigate(TanksListView);
            MainWindow.ShowUserInfoButton.Visibility = Visibility.Visible;
        }

        public void NavigateToLoginView()
        {
            MainWindow.FrameBody.NavigationService.Navigate(LoginView);
        }
    }
}