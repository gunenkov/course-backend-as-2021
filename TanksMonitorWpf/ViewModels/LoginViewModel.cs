﻿using System.Windows.Controls;
using System.Windows.Input;
using TanksMonitorWpf.Helpers.Commands.Asynchronous;
using TanksMonitorWpf.Services.Abstractions;
using TanksMonitorWpf.ViewModels.Abstractions;

namespace TanksMonitorWpf.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private bool authorizationError;


        private string userName;

        public LoginViewModel(IUserService userService)
        {
            AuthorizeCommand = new AsyncCommandWithParameter(async passwordBox =>
                AuthorizationError = !await userService.Authorize(userName, (passwordBox as PasswordBox)?.Password));
        }

        public ICommand AuthorizeCommand { get; set; }

        public bool AuthorizationError
        {
            get => authorizationError;
            set
            {
                authorizationError = value;
                OnPropertyChanged(nameof(AuthorizationError));
                OnPropertyChanged(nameof(StatusText));
            }
        }

        public string UserName
        {
            get => userName;
            set
            {
                userName = value;
                OnPropertyChanged(nameof(UserName));
            }
        }

        public string StatusText => authorizationError ? "Ошибка авторизации" : "Введите учетные данные";
    }
}