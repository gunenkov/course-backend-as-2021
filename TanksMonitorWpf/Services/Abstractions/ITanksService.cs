﻿using Shared.Data.Models;

namespace TanksMonitorWpf.Services.Abstractions
{
    public interface ITanksService : IDataService<Tank>
    {
    }
}