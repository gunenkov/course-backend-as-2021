﻿using System.Threading.Tasks;

namespace TanksMonitorWpf.Services.Abstractions
{
    public interface IUserService
    {
        string UserName { get; set; }
        string Token { get; set; }
        Task<bool> IsAuthorized();
        Task<bool> Authorize(string userName, string password);
    }
}