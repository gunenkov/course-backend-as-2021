﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Shared.Data.Models.Abstractions;

namespace TanksMonitorWpf.Services.Abstractions
{
    public interface IDataService<T> where T : Entity
    {
        Task<IList<T>> GetData();
        Task UpdateItem(T item);
        Task<bool> DeleteItem(T item);
    }
}