﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Shared.Data.Models.DTO;
using Shared.Services.Serialization;
using TanksMonitorWpf.Services.Abstractions;
using TanksMonitorWpf.ViewModels;

namespace TanksMonitorWpf.Services
{
    public class UserService : IUserService
    {
        private readonly IConfiguration configuration;
        private readonly HttpClient httpClient;
        private readonly JsonSerializationService jsonSerializationService;
        private readonly MainWindowViewModel mainWindowViewModel;

        public UserService(JsonSerializationService jsonSerializationService, IConfiguration configuration,
            IHttpClientFactory httpClientFactory,
            MainWindowViewModel mainWindowViewModel)
        {
            this.jsonSerializationService = jsonSerializationService;
            this.configuration = configuration;
            httpClient = httpClientFactory.CreateClient("Client");
            this.mainWindowViewModel = mainWindowViewModel;
        }

        // TODO можно получать объект пользователя и в приложении добавить вывод информации о нем
        public string UserName { get; set; }
        public string Token { get; set; }

        public async Task<bool> IsAuthorized()
        {
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {Token}");

            var response = await httpClient.GetAsync(configuration["CurrentUserUrl"]);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                UserName = await response.Content.ReadAsStringAsync();
                return true;
            }

            UserName = null;
            return false;
        }

        public async Task<bool> Authorize(string userName, string password)
        {
            var body = new UserDto {UserName = userName, Password = password};
            var content = new StringContent(jsonSerializationService.SearializeObject(body), Encoding.UTF8,
                "application/json");
            var response = await httpClient.PostAsync(configuration["AuthUrl"], content);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                var user = jsonSerializationService.DesearializeObject<UserWithTokenDto>(responseBody);
                Token = user.Token;
                UserName = userName;
                mainWindowViewModel.NavigateToTanksListView();
                mainWindowViewModel.UserName = userName;
                return true;
            }

            UserName = null;
            return false;
        }
    }
}