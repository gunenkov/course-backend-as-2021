﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Shared.Data.Models;
using Shared.Data.Models.DTO;
using Shared.Services.Serialization;
using TanksMonitorWpf.Services.Abstractions;

namespace TanksMonitorWpf.Services
{
    public class TanksService : ITanksService
    {
        private readonly IConfiguration configuration;
        private readonly HttpClient httpClient;
        private readonly JsonSerializationService jsonSerializationService;
        private readonly IUserService userService;

        public TanksService(IHttpClientFactory httpClientFactory, JsonSerializationService jsonSerializationService,
            IConfiguration configuration, IUserService userService)
        {
            httpClient = httpClientFactory.CreateClient("Client");
            this.jsonSerializationService = jsonSerializationService;
            this.configuration = configuration;
            this.userService = userService;
        }

        public async Task<IList<Tank>> GetData()
        {
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {userService.Token}");

            var response = await httpClient.GetAsync(configuration["GetTanksUrl"]);
            if (response.StatusCode == HttpStatusCode.OK)
                return jsonSerializationService.DesearializeObject<ObservableCollection<Tank>>(
                    await response.Content.ReadAsStringAsync());
            return new ObservableCollection<Tank>();
        }

        public async Task UpdateItem(Tank item)
        {
            try
            {
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {userService.Token}");
                var body = new CreateOrUpdateTankDto
                    {Name = item.Name, Volume = item.Volume, MaxVolume = item.MaxVolume};
                var content = new StringContent(jsonSerializationService.SearializeObject(item), Encoding.UTF8,
                    "application/json");

                await httpClient.PutAsync($"{configuration["DeleteTankUrl"]}/{item.Id}", content);
            }

            catch (Exception ex)
            {
                // TODO сделать что-то полезное
            }
        }

        public async Task<bool> DeleteItem(Tank item)
        {
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {userService.Token}");

            var response = await httpClient.DeleteAsync($"{configuration["DeleteTankUrl"]}/{item.Id}");
            return response.StatusCode == HttpStatusCode.OK;
        }
    }
}