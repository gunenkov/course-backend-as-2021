﻿using System.Text.Json;
using Microsoft.Extensions.DependencyInjection;
using Shared.Services.Serialization;

namespace TanksMonitorWpf.Extensions
{
    public static class JsonSerializationServiceExtensions
    {
        public static void AddJsonSerizalizationService(this IServiceCollection services)
        {
            JsonSerializationService.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            services.AddSingleton<JsonSerializationService>();
        }
    }
}