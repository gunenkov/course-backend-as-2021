﻿using Shared.Data.Models.Identity;

namespace WebApi.Services
{
    public interface IJwtService
    {
        string GenerateJwtToken(User user);
    }
}