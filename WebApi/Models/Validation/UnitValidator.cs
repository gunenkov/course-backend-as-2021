﻿using FluentValidation;
using Shared.Data.Models.DTO;

namespace WebApi.Models.Validation
{
    public class UnitValidator : AbstractValidator<CreateOrUpdateUnitDto>
    {
        public UnitValidator()
        {
            RuleFor(unit => unit.Name).NotEmpty().WithMessage("Название не может быть пустым");
        }
    }
}