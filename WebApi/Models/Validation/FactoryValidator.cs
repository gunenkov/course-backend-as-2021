﻿using FluentValidation;
using Shared.Data.Models.DTO;

namespace WebApi.Models.Validation
{
    public class FactoryValidator : AbstractValidator<CreateOrUpdateFactoryDto>
    {
        public FactoryValidator()
        {
            RuleFor(factory => factory.Name).NotEmpty().WithMessage("Название не может быть пустым");
            RuleFor(factory => factory.Description).NotEmpty().WithMessage("Описание не может быть пустым");
        }
    }
}