﻿using FluentValidation;
using Shared.Data.Models.DTO;

namespace WebApi.Models.Validation
{
    public class TankValidator : AbstractValidator<CreateOrUpdateTankDto>
    {
        public TankValidator()
        {
            RuleFor(tank => tank.Name).NotEmpty().WithMessage("Название не может быть пустым");
            RuleFor(tank => tank.Volume).LessThanOrEqualTo(tank => tank.MaxVolume)
                .WithMessage("Текущий объем резервуара не может превышать максимальный объем");
        }
    }
}