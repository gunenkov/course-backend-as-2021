﻿using AutoMapper;
using Shared.Data.Models;
using Shared.Data.Models.DTO;

namespace WebApi.Models.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Unit, UnitWithTanksDto>();
            CreateMap<Unit, UnitDto>();
            CreateMap<CreateOrUpdateUnitDto, Unit>();

            CreateMap<Tank, TankDto>();
            CreateMap<CreateOrUpdateTankDto, Tank>();

            CreateMap<Factory, FactoryDto>();
            CreateMap<CreateOrUpdateFactoryDto, Factory>();
            CreateMap<Factory, FactoryWithUnitsAndTanksDto>();
            CreateMap<UserDto, UserWithTokenDto>();
        }
    }
}