﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Shared.Data.Repositories.Abstractions;

namespace WebApi.HostedServices
{
    public class RandomTankVolumeService : IHostedService, IDisposable
    {
        private readonly ILogger<RandomTankVolumeService> logger;
        private readonly IServiceScopeFactory scopeFactory;
        private Timer timer;

        public RandomTankVolumeService(IServiceScopeFactory scopeFactory, ILogger<RandomTankVolumeService> logger)
        {
            this.scopeFactory = scopeFactory;
            this.logger = logger;
        }

        public void Dispose()
        {
            timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(5));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public async void DoWork(object state)
        {
            using var scope = scopeFactory.CreateScope();
            {
                var tanksRepository = scope.ServiceProvider.GetRequiredService<ITanksRepository>();
                foreach (var tank in tanksRepository.GetAll().ToList())
                {
                    var volume = await tanksRepository.RandomUpdateVolume(tank.Id);

                    if (volume > tank.MaxVolume)
                        logger.LogWarning($"Превышен максимальный объем резервуара {tank.Name}. " +
                                          $"Текущий объем: {volume}. Максимальный объем: {tank.MaxVolume}");
                }
            }
        }
    }
}