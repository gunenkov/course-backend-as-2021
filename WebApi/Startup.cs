using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Shared.Data.Models.Identity;
using Shared.Data.Repositories;
using Shared.Data.Repositories.Abstractions;
using Shared.Data.UnitsOfWork;
using WebApi.Extensions;
using WebApi.HostedServices;
using WebApi.Models.Validation;
using WebApi.Services;

namespace WebApi
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IHostEnvironment environment)
        {
            var configurationBuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", true, true);
            configuration = configurationBuilder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<DatabaseContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"),
                    builder => builder.MigrationsAssembly(nameof(Shared))));

            services.AddAutoMapper(typeof(Startup).Assembly);
            services.AddScoped<IFactoriesRepository, FactoriesRepository>();
            services.AddScoped<IUnitsRepository, UnitsRepository>();
            services.AddScoped<ITanksRepository, TanksRepository>();
            services.AddScoped<IUsersRepository, UsersReporitory>();
            services.AddSingleton<FactoryValidator>();
            services.AddSingleton<UnitValidator>();
            services.AddSingleton<TankValidator>();
            services.AddSingleton<IJwtService, JwtService>();
            services.AddSingleton<IPasswordHasher<User>, PasswordHasher<User>>();
            services.AddHostedService<RandomTankVolumeService>();
            services.AddSwaggerDoc();
            services.AddJwtAuthorization(configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "CourseAS API"));

            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}