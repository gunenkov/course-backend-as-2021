﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared.Data.Models;
using Shared.Data.Models.DTO;
using Shared.Data.Repositories.Abstractions;
using WebApi.Models.Validation;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnitController : ControllerBase
    {
        private readonly IFactoriesRepository factoriesRepository;
        private readonly IMapper mapper;
        private readonly IUnitsRepository unitsRepository;
        private readonly UnitValidator validator;

        public UnitController(IUnitsRepository unitsRepository, IFactoriesRepository factoriesRepository,
            IMapper mapper, UnitValidator validator)
        {
            this.unitsRepository = unitsRepository;
            this.factoriesRepository = factoriesRepository;
            this.mapper = mapper;
            this.validator = validator;
        }

        [Authorize(Roles = "Administrator,Operator")]
        [Route("all")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<IList<UnitDto>> GetAll()
        {
            return Ok(mapper.Map<IList<UnitDto>>(unitsRepository.GetAll()));
        }

        [Authorize(Roles = "Administrator,Operator")]
        [Route("{unitId}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UnitWithTanksDto>> GetUnitWithTanks(int unitId)
        {
            var entity = await unitsRepository.GetUnitWithTanks(unitId);
            if (entity == null) return NotFound("Искомый объект не найден");

            return Ok(mapper.Map<UnitWithTanksDto>(entity));
        }

        [Authorize(Roles = "Administrator")]
        [Route("factory/{factoryId}")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CreateOrUpdateFactoryDto>> Add(CreateOrUpdateUnitDto entityDto, int factoryId)
        {
            var validationResults = await validator.ValidateAsync(entityDto);
            if (!validationResults.IsValid) return BadRequest(validationResults.Errors);

            var factory = await factoriesRepository.GetById(factoryId);
            if (factory == null) return NotFound("Завод не найден");

            var entity = mapper.Map<Unit>(entityDto);
            entity.FactoryId = factoryId;
            await unitsRepository.Add(entity);
            return Ok(entityDto);
        }

        [Authorize(Roles = "Administrator")]
        [Route("{unitId}")]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CreateOrUpdateFactoryDto>> Update(CreateOrUpdateUnitDto entityDto, int unitId)
        {
            var validationResults = await validator.ValidateAsync(entityDto);
            if (!validationResults.IsValid) return BadRequest(validationResults.Errors);

            var existingEntity = await unitsRepository.GetById(unitId);
            if (existingEntity == null) return NotFound("Модифицируемый элемент не найден");

            await unitsRepository.Update(entityDto, existingEntity);
            return Ok(entityDto);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{unitId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> Delete(int unitId)
        {
            var existingEntity = await unitsRepository.GetById(unitId);
            if (existingEntity == null) return NotFound("Удаляемый элемент не найден");
            await unitsRepository.Delete(unitId);
            return Ok();
        }
    }
}