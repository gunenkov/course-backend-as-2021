﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared.Data.Models;
using Shared.Data.Models.DTO;
using Shared.Data.Repositories.Abstractions;
using WebApi.Models.Validation;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TankController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly ITanksRepository tanksRepository;
        private readonly IUnitsRepository unitsRepository;
        private readonly TankValidator validator;

        public TankController(ITanksRepository tanksRepository, IUnitsRepository unitsRepository, IMapper mapper,
            TankValidator validator)
        {
            this.tanksRepository = tanksRepository;
            this.unitsRepository = unitsRepository;
            this.mapper = mapper;
            this.validator = validator;
        }

        [Authorize(Roles = "Administrator, Operator")]
        [Route("all")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<IList<TankDto>> GetAll()
        {
            return Ok(mapper.Map<IList<TankDto>>(tanksRepository.GetAll()));
        }

        [Authorize(Roles = "Administrator, Operator")]
        [Route("{tankId}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<TankDto>> Get(int tankId)
        {
            var entity = await tanksRepository.GetById(tankId);
            if (entity == null) return NotFound("Искомый объект не найден");

            return Ok(mapper.Map<TankDto>(entity));
        }

        [Authorize(Roles = "Administrator")]
        [Route("unit/{unitId}")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CreateOrUpdateTankDto>> Add(CreateOrUpdateTankDto entityDto, int unitId)
        {
            var validationResults = await validator.ValidateAsync(entityDto);
            if (!validationResults.IsValid) return BadRequest(validationResults.Errors);

            var unit = await unitsRepository.GetById(unitId);
            if (unit == null) return NotFound("Установка не найдена");

            var entity = mapper.Map<Tank>(entityDto);
            entity.UnitId = unitId;
            await tanksRepository.Add(entity);
            return Ok(entityDto);
        }

        [Authorize(Roles = "Administrator")]
        [Route("{tankId}")]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CreateOrUpdateTankDto>> Update(CreateOrUpdateTankDto entityDto, int tankId)
        {
            var validationResults = await validator.ValidateAsync(entityDto);
            if (!validationResults.IsValid) return BadRequest(validationResults.Errors);

            var existingEntity = await tanksRepository.GetById(tankId);
            if (existingEntity == null) return NotFound("Модифицируемый элемент не найден");

            await tanksRepository.Update(entityDto, existingEntity);
            return Ok(entityDto);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{tankId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> Delete(int tankId)
        {
            var existingEntity = await tanksRepository.GetById(tankId);
            if (existingEntity == null) return NotFound("Удаляемый элемент не найден");
            await tanksRepository.Delete(tankId);
            return Ok();
        }
    }
}