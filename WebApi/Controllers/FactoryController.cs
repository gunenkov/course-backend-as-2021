﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared.Data.Models;
using Shared.Data.Models.DTO;
using Shared.Data.Repositories.Abstractions;
using WebApi.Models.Validation;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FactoryController : ControllerBase
    {
        private readonly IFactoriesRepository factoriesRepository;
        private readonly IMapper mapper;
        private readonly FactoryValidator validator;

        public FactoryController(IFactoriesRepository factoriesRepository, IMapper mapper, FactoryValidator validator)
        {
            this.factoriesRepository = factoriesRepository;
            this.mapper = mapper;
            this.validator = validator;
        }

        [Authorize(Roles = "Administrator,Operator")]
        [Route("all")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<IList<FactoryDto>> GetAll()
        {
            return Ok(mapper.Map<IList<FactoryDto>>(factoriesRepository.GetAll()));
        }

        [Authorize(Roles = "Administrator,Operator")]
        [Route("{factoryId}")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<FactoryWithUnitsAndTanksDto>> GetFactory(int factoryId)
        {
            var entity = await factoriesRepository.GetFactoryWithUnitsAndTanks(factoryId);
            if (entity == null) return NotFound("Искомый объект не найден");

            return Ok(mapper.Map<FactoryWithUnitsAndTanksDto>(entity));
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CreateOrUpdateFactoryDto>> Add(CreateOrUpdateFactoryDto entityDto)
        {
            var validationResults = await validator.ValidateAsync(entityDto);
            if (!validationResults.IsValid) return BadRequest(validationResults.Errors);

            var entity = mapper.Map<Factory>(entityDto);
            await factoriesRepository.Add(entity);
            return Ok(entityDto);
        }

        [Authorize(Roles = "Administrator")]
        [Route("{factoryId}")]
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CreateOrUpdateFactoryDto>> Update(CreateOrUpdateFactoryDto entityDto,
            int factoryId)
        {
            var existingEntity = await factoriesRepository.GetById(factoryId);
            if (existingEntity == null) return NotFound("Модифицируемый элемент не найден");

            var validationResults = await validator.ValidateAsync(entityDto);
            if (!validationResults.IsValid) return BadRequest(validationResults.Errors);

            await factoriesRepository.Update(entityDto, existingEntity);
            return Ok(entityDto);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{factoryId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> Delete(int factoryId)
        {
            var existingEntity = await factoriesRepository.GetById(factoryId);
            if (existingEntity == null) return NotFound("Удаляемый элемент не найден");
            await factoriesRepository.Delete(factoryId);
            return Ok();
        }
    }
}