﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Shared.Data.Models.DTO;
using Shared.Data.Models.Identity;
using Shared.Data.Repositories.Abstractions;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly IJwtService jwtService;
        private readonly IMapper mapper;
        private readonly IPasswordHasher<User> passwordHasher;
        private readonly IUsersRepository usersRepository;

        public UserController(IUsersRepository usersRepository, IMapper mapper, IConfiguration configuration,
            IPasswordHasher<User> passwordHasher, IJwtService jwtService)
        {
            this.usersRepository = usersRepository;
            this.mapper = mapper;
            this.configuration = configuration;
            this.jwtService = jwtService;
            this.passwordHasher = passwordHasher;
        }

        [Route("auth")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserWithTokenDto>> Authorize([FromBody] UserDto userDto)
        {
            var user = await usersRepository.GetByUserNameWithRole(userDto.UserName);
            if (user == null) return Unauthorized("Пользователь не найден");

            var passwordVerificationResult =
                new PasswordHasher<User>().VerifyHashedPassword(user, user.Password, userDto.Password);
            if (passwordVerificationResult != PasswordVerificationResult.Success)
                return Unauthorized("Неверный пароль");

            var userWithToken = mapper.Map<UserWithTokenDto>(userDto);
            userWithToken.Token = jwtService.GenerateJwtToken(user);

            return Ok(userWithToken);
        }

        [Authorize]
        [Route("current")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<string> GetCurrentUserInfo()
        {
            return Ok(HttpContext.User.Identity.Name);
        }

        [Authorize]
        [Route("password/update")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> UpdatePassword(UserUpdatePasswordDto userUpdatePasswordDto)
        {
            if (userUpdatePasswordDto.UserName != HttpContext.User.Identity.Name)
                return StatusCode(403, "Попытка изменить пароль другого пользователя");

            var user = await usersRepository.GetByUserName(userUpdatePasswordDto.UserName);
            if (user == null) return Unauthorized("Пользователь не найден");

            var passwordVerificationResult =
                passwordHasher.VerifyHashedPassword(user, user.Password, userUpdatePasswordDto.CurrentPassword);
            if (passwordVerificationResult != PasswordVerificationResult.Success)
                return Unauthorized("Неверный пароль");

            await usersRepository.UpdatePassword(user,
                passwordHasher.HashPassword(user, userUpdatePasswordDto.NewPassword));
            return Ok();
        }
    }
}