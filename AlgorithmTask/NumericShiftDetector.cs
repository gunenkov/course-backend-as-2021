﻿namespace AlgorithmTask
{
    public class NumericShiftDetector
    {
        /// <summary>
        ///     Метод принимает массив, составленный из отсортированного набора чисел, "сдвинутого" на некоторое количество позиций
        /// </summary>
        /// <param name="initialArray">Исходный массив</param>
        /// <returns>Величина "сдвига" слева</returns>
        public int GetShiftPosition(int[] initialArray)
        {
            var leftIndex = 0;
            var rightIndex = initialArray.Length - 1;

            while (leftIndex < rightIndex)
            {
                // исходим из того, что части массива упорядочены
                if (initialArray[leftIndex] < initialArray[rightIndex]) return leftIndex;

                var middleIndex = (leftIndex + rightIndex) / 2;

                if (initialArray[middleIndex] >= initialArray[leftIndex])
                    leftIndex = middleIndex + 1;
                else
                    rightIndex = middleIndex;
            }

            return leftIndex;
        }
    }
}